const ROWS = 10;
const COLUMNS = 10;
const MINIMUM = -100;
const MAXIMUM = 100;

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateData() {
    const data = [];
    const minimumNumber = {
        value: MAXIMUM,
        row: 0
    }

    for (let i = 0; i < ROWS; i++) {
        const row = {
            minPositive: MAXIMUM,
            numbers: [],
            countReplaces: 0,
            // haveMinimum: false
        };
        for (let j = 0; j < COLUMNS; j++) {
            const newNumber = getRandomNumber(MINIMUM, MAXIMUM)
            row.numbers.push(newNumber);

            //  check positive minimum number in row
            if (newNumber > 0 && newNumber < row.minPositive) {
                row.minPositive = newNumber
            }

            // check minimum number in row
            if (newNumber < minimumNumber.value) {
                minimumNumber.value = newNumber
                minimumNumber.row = i
            }
        }

        row.countReplaces = getCountReplaces(row.numbers)
        data.push(row);
    }

    // add haveMinimum property in row where have "positive minimum number"
    data[minimumNumber.row].haveMinimum = true

    return data;
}

function getCountReplaces(arr) {
    // count positive and negative numbers in a row
    let countPos = 0; 
    let countNeg = 0; 


    let replaces = 0; 
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > 0) {
            countPos++;
            countNeg = 0;
        } else if (arr[i] < 0) {
            countNeg++;
            countPos = 0;
        } else {
            countPos = 0;
            countNeg = 0;
        }

        // if have 3 positive or free negative change value to 0
        if (countPos === 3 || countNeg === 3) {
            replaces++;
            countPos = 0;
            countNeg = 0;
        }
    }
    return replaces;
}


const array = generateData();

// you can see array for more information
// console.log(array);

for (let i = 0; i < array.length; i++) {
    const rowData = array[i]
    let string = rowData.numbers.join('\t');   
    if(rowData.haveMinimum){
        string += '  *'
    }
    string += '  || minimum positive: ' +  rowData.minPositive
    string += '  || need replace value: ' +  rowData.countReplaces
    console.log(string);
}
